var jsonQuery = require('json-query')
var db = require("./known_data")

exports.hasUserStarted = function (messages, from) {

    var queryFindDone = "[*from=" + from + "&msg~/^!start/i]"

    var data = {
        data: messages,
        allowRegexp: true
    }

    var filtered = jsonQuery(queryFindDone, data).value
    return filtered
}

exports.getDone = function (messages, from) {
    var queryFindDone

    if (from) {
        queryFindDone = "[*from=" + from + "&msg~/^!done/i]"
    }
    else {
        queryFindDone = "[*msg~/^!done/i]"
    }

    var data = {
        data: messages,
        allowRegexp: true
    }

    return jsonQuery(queryFindDone, data).value
}

exports.doneForToday = function (messages, from) {
    var messagesWithDone
    if (from) {
        messagesWithDone = exports.getDone(messages, from)
    }
    else {
        messagesWithDone = exports.getDone(messages)
    }

    var queryFindDates = "[*created:today & from!="+global.ownID+"]"
    var data = {
        data: messagesWithDone,
        locals: {
            today: function (item) {
                return new Date(item).setHours(0, 0, 0, 0) == new Date().setHours(0, 0, 0, 0)
            }
        }
    }
    return jsonQuery(queryFindDates, data).value
}



