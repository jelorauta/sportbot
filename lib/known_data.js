var jsonQuery = require('json-query')

exports.users = []
exports.fields = []

var userData = {
    data:exports.users
}

var fieldData = {
    data:exports.fields
}

exports.initData = function (data) {
    Object.keys(data.users).forEach(usrKey => {
        exports.users.push(data.users[usrKey])
    })

    var networks = data.processes
    Object.keys(networks).forEach(key => {
        Object.keys(networks[key]).forEach(proKey => {
            Object.keys(networks[key][proKey].fields).forEach(fieldKey => {
                exports.fields.push(networks[key][proKey].fields[fieldKey])
            })

        })
    })
}

exports.getUser = function (userId) {
    var query = "[_id=" + userId + "]"
    return jsonQuery(query, userData).value
}

exports.getField = function(fieldID)
{
    var query = "[_id=" + fieldID + "]"
    var data = { data: exports.fields }

    return jsonQuery(query, data).value
}

exports.addUser = function (userObject) {
    console.log(exports.users)
    db.users.push(userObject)
    console.log(exports.users)
}

exports.addField = function (field) {
    console.log(exports.fields)
    db.fields.push(field)
    console.log(exports.fields)
}