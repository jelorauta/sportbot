const socket = require("./socketService")
const consts = require("./consts")

exports.fetchUser = function (userId, discussion, callback) {
    var args = [userId];
    socket.makeRequest(consts.USER_LOAD, args, callback)
}

exports.fetchDiscussion = function (discussion, callback) {
    var args = [[discussion]]
    socket.makeRequest(consts.MESSENGER_LOAD_DISCUSSIONS, args, callback)
}

exports.fetchAllDiscussions = function(callback)
{
    var args = []
    socket.makeRequest(consts.MESSENGER_LOAD_DISCUSSIONS,args,callback)
}

exports.fetchActivity = function (actId, callback) {
    var args = [actId]
    socket.makeRequest(consts.ACTIVITIES_LOAD, args, callback)
}

exports.fetchProcess = function(processID, callback)
{
    var args =[processID]
    socket.makeRequest(consts.PROCESS_LIST,args,callback)
}

exports.leaveDiscussion = function (discID, callback) {
    var args = [discID]
    socket.makeRequest(consts.MESSENGER_LEAVE_DISCUSSION, args, callback)
}

exports.fetchAllMessages = function(discId, callback)
{
    var options = {limit:false}
    var args = [discId, options]
    socket.makeRequest(consts.MESSENGER_LOAD_MESSAGES, args, callback)
}

exports.sendMessage = function (message, discussion, callback) {
    var args = [{
        "tags": [],
        "tagged": [],
        "files": [],
        "msg": message
    },
        discussion]

    socket.makeRequest(consts.MESSENGER_SEND, args, callback)
}