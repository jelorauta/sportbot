for (i = 0; i < 150; i++) {
    var str = ""
    if (i % 3 == 0) {
        str += "fizz"
    }
    if (i % 5 == 0) {
        str += "buzz"
    }
    if (str.length > 0)
        console.log(str)
    else
        console.log(i)
}