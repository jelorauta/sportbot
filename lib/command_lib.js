const calls = require("./server_calls")
const parser = require("./command_parser")
var jsonQuery = require('json-query')
var dataParser = require('./data_parser')
var db = require("./known_data")

exports.Hello = function (msgData) {
    if (msgData === true) {
        return "You hello me, I hello you."
    } else {
        var user = db.getUser(msgData.from)
        if (user) {
            greetUser(null, null, msgData.discussion, user)
        }
        else {
            calls.fetchUser(msgData.from, msgData.discussion, function (request, response) {
                db.addUser(response.data)
                greetUser(request, response, msgData.discussion)
            })
        }

    }
}

exports.Square = function (msgData, params) {
    if (msgData === true) {
        return "This is my demo function. Give me a number and I shall square it! This command takes a parameter. !command:Parameter"
    } else {
        calls.sendMessage("Square of " + params[0] + " is " + (params[0] * params[0]) + ". You're welcome.", msgData.discussion)
    }
}

exports.Help = function (data) {
    if (data === true) {
        return "Show this help message again."
    }
    else {
        parser.sendHelp(data.discussion)
    }
}


exports.Widsom = function (msgData) {
    if (msgData === true) {
        return "I shall tell you a great wisdom."
    }

    var done
    var str
    var options = {
        uri: "https://favqs.com/api/quotes",
        headers: {
            Authorization: "Token token=fe190e77d805dfa81e08932d41e8483c"
        }
    }

    // request(options, function (err, res, body) {
    //     if (!err && res.statusCode === 200) {
    //         var data = JSON.parse(body)
    //         calls.sendMessage("As the great " + data.quotes[1].author + " once said:<br/>" + data.quotes[1].body, msgData.discussion)
    //     }
    //     else {
    //         console.log(err)
    //     }
    // })
}

exports.Plan = function (msgData) {
    if (msgData === true) {
        return "I'll recite the plan linked in this activity."
    }

    calls.fetchDiscussion(msgData.discussion, function (discReq, discRes) {

        if (discRes.data[msgData.discussion].linked_activity) {
            calls.fetchActivity(discRes.data[msgData.discussion].linked_activity, function (actReq, actRes) {
                var latest = actRes.data.history.length - 1
                var fields = Object.keys(actRes.data.history[latest])
                var values = actRes.data.history[latest]
                var message = ""
                fields.forEach(key => {
                    var field = db.getField(key)

                    if (field) {
                        message += field.label + " - " + values[key].value + "<br/>"
                    }
                })
                calls.sendMessage(message, msgData.discussion)
            })
        }
        else {
            calls.sendMessage("Sorry. I can only function in activities. I'll be leaving now. Bye!", msgData.discussion);
            calls.leaveDiscussion(msgData.discussion)
        }
    })
}

exports.Progress = function (msgData, params) {
    if (msgData === true) {
        return "See your current progress in this exercise. <br/><b>!progress:all</b> - Shows the progress of every member in this activity."
    }
    calls.fetchAllMessages(msgData.discussion, function (request, response) {
        if (params && params.indexOf("all") > -1) {
            showProgressForAllUsers(response.data, msgData.discussion)
        }
        else {
            showProgerssForUser(msgData.from, response.data, msgData.discussion)
        }

    })
}

// exports.Start = function (msgData) {
//     if (msgData === true) {
//         return "I will keep track of your progress in this exercise."
//     }
//     calls.fetchAllMessages(msgData.discussion, function (request, response) {
//         var filtered = dataParser.hasUserStarted(response.data, msgData.from)
//         if (filtered) {
//             calls.sendMessage("You have already started this exercise. Call !done to tell me you have completed the exercise.", msgData.discussion)
//         }
//         else {
//             calls.sendMessage("Great! Remember to call !done every time you have completed the exercise.", msgData.discussion)
//         }
//     })
// }

exports.Done = function (msgData) {
    if (msgData === true) {
        return "Use this command to tell me you have finished your exercises for today."
    }
    exports.Progress(msgData)
}

exports.remindUsersMorning = function () {
    calls.fetchAllDiscussions(function (request, response) {
        response.data.forEach(disc => {
            calls.sendMessage("Rise and shine! There's no better way to start the day than a bit of exercise!", disc._id)
        })
    })
}

exports.remindUsersMid = function () {
    calls.fetchAllDiscussions(function (request, response) {
        response.data.forEach(disc => {
            calls.sendMessage("Lets see who has burned that lunch already...", disc._id)
            exports.Progress({ discussion: disc._id }, ["all"])
        })
    })
}

exports.remindUsersEve = function () {
    calls.fetchAllDiscussions(function (request, response) {
        response.data.forEach(disc => {
            calls.sendMessage("Remember you need to exercise today!", disc._id)
            exports.Progress({ discussion: disc._id }, ["all"])
        })
    })
}

exports.remindUsersEnd = function () {
    calls.fetchAllDiscussions(function (request, response) {
        response.data.forEach(disc => {
            calls.sendMessage("End of the day. Time for sleep people. Let's see who did their workouts today...", disc._id)
            exports.Progress({ discussion: disc._id }, ["all"])
        })
    })
}

exports.remindUsersWkend = function () {
    calls.fetchAllDiscussions(function (request, response) {
        response.data.forEach(disc => {
            calls.sendMessage("Happy weekend folks! See you again on Monday!", disc._id)
        })
    })
}








//Function to make to code more readable
function greetUser(request, response, discussion, knownUser) {
    var foundUser
    if (knownUser) {
        foundUser = knownUser.firstname
    }
    else {
        foundUser = response.data.firstname
    }

    const greetings = [
        `yo`,
        `Hello ${foundUser} my dear friend!`,
        `Hey ${foundUser}! Done any exercises today?`,
        `Heyo!`,
        `Nice to hear from you again ${foundUser}!`,
        `Heya!`,
        `not this guy again...`,
        `Mae govannen ${foundUser}`,
        `Beep boop.`,
        `Moromoro!`,
        `Nas beru uhn'adarr ${foundUser}?`,
        `Kon'nichiwa!`,
        `Lok'thar ogar!`,
        `bIpIv'a' ${foundUser}?`,
        `Hi! How are you today ${foundUser}?`,
        `Throm-Ka ${foundUser}`
    ]
    var message = greetings[Math.floor(Math.random() * greetings.length)]
    calls.sendMessage(message, discussion)
}

function showProgerssForUser(userId, messages, discId) {
    var doneCountToday = dataParser.doneForToday(messages, userId).length
    var str
    var user = db.getUser(userId)
    if (doneCountToday == 0) {
        str = "You haven't done the exercise today " + user.firstname + ". Remeber to call !done when you're done so I don't bug you for no reason."
    }
    else if (doneCountToday == 1) {
        str = "Great job " + user.firstname + "! You've completed the exercise for today! No one says that you have to stop now ;)"
    }
    else {
        str = "Wow! You've completed the exercise " + doneCountToday + " times today. Keep it up " + user.firstname + "!"
    }
    calls.sendMessage(str, discId)
}

function showProgressForAllUsers(messages, discId) {

    calls.fetchDiscussion(discId, function (request, response) {
        var progress = {}
        Object.keys(response.data).forEach(key => {
            response.data[key].participants.forEach(user => {
                progress[user] = dataParser.doneForToday(messages, user).length
            })
        })

        var done = ""
        var notDone = ""
        Object.keys(progress).forEach(user => {
            if (progress[user] == 0) {
                var user = db.getUser(user)
                if (user)
                    notDone += user.firstname + "<br/>"
            }
            else {
                var user = db.getUser(user)
                if (user)
                    done += user.firstname + "<br/>"
            }
        })

        var message = ""

        if (notDone.length == 0) {
            message = "Everyone has completed their exercise today! Great job y'all! Keep it up!"
        }
        else if (done.length == 0) {
            message = "No one has yet completed their daily exercise. Step it up people!"
        }
        else {
            message = "Done for today:<br/>" + done + "<br/>Still waiting for:<br/>" + notDone
        }

        calls.sendMessage(message, discId)
    })

}