const socket = require("./socketService.js")
const consts = require("./consts")
const calls = require("./server_calls")
const request = require("request")
const command_lib = require("./command_lib")
var wod_commands = []

wod_commands.hello = command_lib.Hello
wod_commands.square = command_lib.Square
wod_commands.help = command_lib.Help
wod_commands.wisdom = command_lib.Widsom
// wod_commands.plan = command_lib.Plan
wod_commands.progress = command_lib.Progress
// wod_commands.start = command_lib.Start
wod_commands.done = command_lib.Done


exports.sendHelp = function (discId) {
    var str = "These are my supported commands: <br/><br/>"

    var keys = Object.keys(wod_commands).sort();
    for (i = 0; i < keys.length - 1; i++) {
        var key = keys[i]
        str = str.concat("<b>!" + key + "</b> -- " + wod_commands[key](true) + "<br/>")
    }

    str = str.concat("<b>!" + keys[keys.length - 1] + "</b> -- " + wod_commands[keys[keys.length - 1]](true))

    calls.sendMessage(str, discId)
}

exports.handleCommand = function (data) {
    if (data.msg) {
        var findCommand = new RegExp("^![a-zA-z:,0-9]+$", "img")
        var recievedMessage = data.msg
        var found = recievedMessage.match(findCommand)
        var command
        var params

        if (found) {
            if (found.length > 1) {
                calls.sendMessage("I can only handle one command at a time :(", data.discussion)
                return
            }

            command = found[0].replace(/!/, '').toLocaleLowerCase()
            if (command.indexOf(":") > -1) {
                var splitted = command.split(":")
                params = splitted[1].split(",")
                command = splitted[0]
                console.log("Command :" + command + "      Params: " + params)
            }

            if (Object.keys(wod_commands).indexOf(command) > -1) {
                if (params) {
                    wod_commands[command](data,params)
                }
                else {
                    wod_commands[command](data)
                }
                console.log("Found a matching command for \"" + command + "\"")
            }
            else {
                console.log(command)
                calls.sendMessage("I didn't understand what you meant with <b>\"" + command + "\"<b>", data.discussion)
                wod_commands.help(data)
            }
        }
    }

}