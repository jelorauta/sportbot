const http = require('http')
const handler = require('./handler.js')
const io = require('socket.io-client')
const consts = require("./consts")

var id_counter = 0;
var hlrKey = "";
var requests = {};
var callbacks = {};
var discID = ""

const devPass = "wodbot";
const devEmail = "wod@bot.com";
const devURL = "https://dev.hailer.com"

const passwordProd = "SportBot2017"
const emailProd = "sport@bot.com"
const URLProd = "https://www.hailer.com"


var socket

exports.init = function (inputEmail, inputPSWD) {

    if (inputEmail === "DEV" && inputPSWD === "pass") {
        socket = io.connect(devURL)

        socket.on('connect', function () {
            console.log("Connected! Sending login credentials.")

            var args = {
                "keep": true,
                "pass": devPass,
                "user": devEmail
            }

            var args_array = []
            args_array.push(args)
            args_array.push(null)

            exports.makeRequest(consts.CORE_LOGIN, args_array)
        })
    }
    else {
        socket = io.connect(URLProd)

        socket.on('connect', function () {
            console.log("Connected! Sending login credentials.")

            var args = {
                "keep": true,
                "pass": inputPSWD,
                "user": inputEmail
            }

            var args_array = []
            args_array.push(args)
            args_array.push(null)

            exports.makeRequest(consts.CORE_LOGIN, args_array)

        })
    }

    socket.on('rpc-response', function (res) {
        if (res.data.sig) {
            if (consts.SHOW_ALL_NETWORK_DATA) {
                console.log("Signal data")
                console.log(res.data)
            }

            handler.handleSignal(res.data.data.signal, res.data.data)
        }
        else {
            if (consts.SHOW_ALL_NETWORK_DATA)
                console.log("RPC-RESPONSE: ", res)

            if (res.data.hasOwnProperty("err")) {
                handler.handleError(requests[res.data.err], res.data)
            }
            else {
                var data = res.data;
                var ack = res.data.ack;
                if (requests[ack].callback) {
                    requests[ack].callback(requests[ack], data)
                }
                else {
                    handler.handleResponse(requests[ack], data)
                }
            }
        }

    })
}

exports.close = function(){
    socket.close()
}

exports.makeRequest = function (command, args, callback) {
    var request = {
        "id": id_counter,
        "op": command,
        "args": args
    }

    if (callback) {
        requests[id_counter] = { "request": request, "callback": callback }
    }
    else {
        requests[id_counter] = request
    }

    id_counter++

    console.log("Making request: %s", request.op)
    if (consts.SHOW_ALL_NETWORK_DATA)
        console.log("Arguments: ", request.args)
    socket.emit("rpc-request", request)
}