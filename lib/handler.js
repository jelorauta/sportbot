var socket = require("./socketService.js")
var commandHandler = require("./command_parser.js")
var commands = require("./command_lib")
var db = require("./known_data")
const consts = require("./consts")
var calls = require("./server_calls")
var schedule = require("node-schedule")
global.ownID


exports.handleResponse = function (request, responseData) {

    var data = responseData.data
    if (!request || !request.op) {
        console.warn("SOMETHING FUCKY HAPPENED! CHECK TO REQUEST DATA")
        console.warn(request)
        console.warn("CHECK OUT THE RESPONSE AS WELL")
        console.warn(responseData)
    }
    var command = request.op
    var args = request.args

    console.log("HANDLING %s: ", command)
    if (consts.SHOW_ALL_NETWORK_DATA)
        console.log(responseData)

    switch (command) {
        case consts.CORE_LOGIN:
            socket.makeRequest(consts.CORE_SIGNALS, [true])
            socket.makeRequest(consts.CORE_INIT, [])
            break;

        case consts.CORE_INIT:
            global.ownID = data.user._id
            db.initData(data)
            initTimers()
            break

        case consts.MESSENGER_LOAD_MESSAGES:
            if (data.msg && global.ownID !== data.from) { commandHandler.handleCommand(data) }
            else {
                var meta = data.meta
                var type = data.type
                if (type == "discussion.invites" && meta[0] === global.ownID) {
                    calls.sendMessage("Hello, your friendly neighbourhood Sport Bot here! Consider calling !help if you don't know how I operate.", data.discussion)
                }
            }
            break;

        case consts.USER_LOAD:
            db.addUser(data.data)
            console.log(args)
            break;
    }

}

exports.handleError = function (request, data) {
    console.log(data.data)
    console.log("Provide valid credentials to start the service.")
    socket.close();
    return
}

exports.handleSignal = function (signal, data) {
    console.log("Received a signal: ", signal)
    console.log("Signal data:", data)

    switch (signal) {
        case consts.SIGNAL_NEW_MESSAGE:
            {
                var args = [data.meta.discussion, { "msg_id": data.meta.msg_id }]

                if (data.meta.uid !== global.ownID)
                    socket.makeRequest(consts.MESSENGER_LOAD_MESSAGES, args)
            }
    }
}

function initTimers() {
    var morning = new schedule.RecurrenceRule();
    morning.dayOfWeek = [0, new schedule.Range(0, 6)]
    morning.hour = 8
    morning.minute = 0

    var midday = new schedule.RecurrenceRule();
    midday.dayOfWeek = [0, new schedule.Range(0, 6)]
    midday.hour = 12
    midday.minute = 0

    var evening = new schedule.RecurrenceRule();
    evening.dayOfWeek = [new schedule.Range(0, 6)]
    evening.hour = [new schedule.Range(16, 19)]
    evening.minute = 0

    var end = new schedule.RecurrenceRule();
    end.dayOfWeek = [new schedule.Range(0, 6)]
    end.hour = 21
    end.minute = 18

    var weekend = new schedule.RecurrenceRule();
    weekend.dayOfWeek = 5
    weekend.hour = 22
    weekend.minute = 1


    var now = new Date(Date.now());
    var hour = new Date(Date.now+(1000*60*60));
    var annoyHenkka = {
        start:now,
        end:hour,
        rule:"*/5 * * * *"
    }

    // var mor = schedule.scheduleJob(morning, commands.remindUsersMorning)
    // var mid = schedule.scheduleJob(midday, commands.remindUsersMid)
    // var eve = schedule.scheduleJob(evening, commands.remindUsersEve)
    // var en = schedule.scheduleJob(end, commands.remindUsersEnd)

    var annoy = schedule.scheduleJob(annoyHenkka, commands.remindUsersEve);

    // var wken = schedule.scheduleJob(weekend, commands.remindUsersWkend)
}